#include "AlarmClock.h"

AlarmClock::AlarmClock(int day, int mon, int year, int hour, int min, string SignalName)
{
	m_mon = mon;
	m_day = day;
	m_year = year;
	m_hour = hour;
	m_min = min;
	m_SignalName = SignalName;
}

void AlarmClock::GetTime()
{
	cout << " ---------------" << endl;

	printf("  Date %02d.%02d.%02d ", t_m->tm_mday, t_m->tm_mon + 1, t_m->tm_year - 100);
	printf("\n  Time %02d:%02d:%02d ", t_m->tm_hour, t_m->tm_min, t_m->tm_sec);

	cout << "\n ---------------" << endl;
}

bool AlarmClock::CheckTime()
{
	if (t_m->tm_mon + 1 == m_mon)
		if (t_m->tm_mday == m_day)
			if (t_m->tm_year - 100 == m_year)
				if (t_m->tm_hour == m_hour)
					if (t_m->tm_min == m_min)
						return true;

	return false;
}

bool AlarmClock::Wrong()
{
	if (t_m->tm_mon + 1 > m_mon)return true;
	if (t_m->tm_mday > m_day) return true;
	if (t_m->tm_year - 100 > m_year) return true;
	if (t_m->tm_hour > m_hour) return true;
	if (t_m->tm_hour == m_hour && t_m->tm_min > m_min) return true;

	return false;
}

bool AlarmClock::ShutDown()
{
	if (_kbhit)
	{
		int key = _getch();

		if (key == 13)
		{
			cout << "   < Stop >\n" << endl;

			PlaySound(NULL, NULL, SND_FILENAME | SND_ASYNC);

			return true;
		}

		if (key == 32)
		{
			cout << "  < Put off signal > \n" << endl;

			PlaySound(NULL, NULL, SND_FILENAME | SND_ASYNC);

			return false;
		}
	}
}

int AlarmClock::LifeCycle()
{
	while (1)
	{
		system("cls");
		cout << "\n   ALARM CLOCK " << endl;

		t = time(NULL);
		t_m = localtime(&t);

		GetTime();

		if (!Wrong())
		{
			cout << " Set Music: " << m_SignalName << endl;
			cout << "\n > Next time for signal\n " << endl;

			printf("  %02d.%02d.%02d ", m_day, m_mon, m_year);
			printf("  %02d:%02d ", m_hour, m_min);

			if (CheckTime())
			{
				cout << "\n Press <ENTER> to stop \n Press <SPACE> to put off \n" << endl;

				wstring w = std::wstring(m_SignalName.begin(), m_SignalName.end());
				w.c_str();

				PlaySound(w.c_str(), NULL, SND_FILENAME | SND_ASYNC);

				if (ShutDown()) return 0;

				else
				{
					if (m_min > 54)
					{
						m_min = 10 - (m_min - 50);
						m_hour = m_hour + 1;
					}

					else m_min = m_min + 10;
				}
			}
		}
		else
		{
			cout << "\n < Wrong or Empty Schedule > \n" << endl;

			return 0;
		}

		this_thread::sleep_for(chrono::milliseconds(1000));
	}
}
