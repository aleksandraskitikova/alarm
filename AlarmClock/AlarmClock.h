#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "windows.h"
#include "mmsystem.h"
#include <string.h>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <chrono>
#include <thread>
#include <fstream>
#include <conio.h>
#pragma comment(lib,"winmm.lib")
using namespace std;

class AlarmClock {

public:
	AlarmClock(int day, int mon, int year, int hour, int min, string SignalName);
	~AlarmClock() {}
	void GetTime();
	bool CheckTime();
	bool Wrong();
	bool ShutDown();
	int LifeCycle();

private:
	time_t t;
	struct tm* t_m;
	int m_mon, m_day, m_year, m_hour, m_min;
	string m_SignalName;

};



