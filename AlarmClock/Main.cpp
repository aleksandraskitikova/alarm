﻿#include "AlarmClock.h"

int main(char argc, char** argv)
{
	string SignalName;
	int Schedule[7][5];

	for (int i = 0; i < 7; i++)
		for (int j = 0; j < 5; j++)
			Schedule[i][j] = 0;

	freopen("Schedule.txt", "r", stdin);

	cin >> SignalName;

	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 5; j++)
			cin >> Schedule[i][j];

		if (Schedule[i][0] > 31 || Schedule[i][1] > 12 || Schedule[i][3] > 23 || Schedule[i][4] > 59)
		{
			cout << "\n > Wrong Time in Schedule " << endl;

			return 0;
		}

		AlarmClock alarm(Schedule[i][0], Schedule[i][1], Schedule[i][2], Schedule[i][3], Schedule[i][4], SignalName);
		alarm.LifeCycle();

		system("pause");

		cout << endl;
	}

	return 0;
}